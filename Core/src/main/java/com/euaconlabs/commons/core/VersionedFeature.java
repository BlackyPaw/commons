package com.euaconlabs.commons.core;

import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * The base class for creating version dependent features like protocol-hacks and
 * workarounds. Every versioned feature may register one or more
 * {@link com.euaconlabs.commons.core.IVersionedImpl implementations} of which the best-matching one
 * will be chosen automatically given the protocol version of a given user. The best-matching
 * implementation is always considered to be the one with the version number closest to that one
 * of a given user but not higher than the version of the protocol the user is connected with.
 *
 * @author BlackyPaw
 */
public class VersionedFeature<T extends IVersionedImpl> {

    /**
     * The list of registered implementations available for the versioned feature.
     */
    private List<T> implementations = new ArrayList<T>();

    /**
     * Constructor for a versioned feature object. It may be given zero or more
     * possible implementations.
     * @param impl A set of implementations which shall be auto-registered to the feature.
     */
    protected VersionedFeature( Class<? extends T> ... impl ) {
        for ( Class<? extends T> c : impl ) {
            this.registerImplementation( c );
        }
    }

    /**
     * Registers the given implementation for the versioned feature. This will automatically
     * create an instance of the implementation which will be kept internally until needed.
     * In order to create an instance of the implementation the implementation's class is assumed
     * to have a default constructor declared.
     * @param impl The class of the implementation to register.
     */
    protected boolean registerImplementation( Class<? extends T> impl ) {

        try {

            Constructor<? extends T> c = impl.getDeclaredConstructor( );
            c.setAccessible( true );
            T instance = c.newInstance();
            this.implementations.add( instance );

            return true;

        } catch ( IllegalAccessException e ) {
            e.printStackTrace();
            return false;
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
            return false;
        } catch ( InstantiationException e ) {
            e.printStackTrace();
            return false;
        } catch ( NoSuchMethodException e ) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * Finds a suitable implementation for the given player. This will inspect the protcol
     * the player is connecting with in order to choose the most appropriate implementation.
     * @param player The player an implementation is to be retrieved for.
     * @return The best implementation found for the given player.
     * @throws java.lang.IllegalStateException If not a single implementation is registered
     *      at the point of the method's invocation an IllegalStateException will be thrown.
     */
    protected T findImplementation ( Player player ) {

        if ( this.implementations.isEmpty() )
            throw new IllegalStateException( "No implementations registered!" );

        // This is the best protocol version we may have an implementation for:
        final int targetVersion = ( (CraftPlayer) player ).getHandle().playerConnection.networkManager.getVersion();

        int implVersion = -1;
        T implInstance = null;

        for ( T impl : this.implementations ) {
            if ( impl.getVersion() == targetVersion ) // We're done
                return impl;

            if ( impl.getVersion() > implVersion && impl.getVersion() <= targetVersion ) {
                implVersion = impl.getVersion();
                implInstance = impl;
            }
        }

        return implInstance;

    }




}
