package com.euaconlabs.commons.core;

/**
 * The basic interface for creating a version specific implementation of some feature
 * needed to be version-independent. It
 *
 * @author BlackyPaw
 */
public interface IVersionedImpl {

    /**
     * Gets the protocol version the implementation is suitable for. A complete list of version
     * numbers may be obtained here: <a href="http://wiki.vg/Protocol_version_numbers">Protocol Version Numbers</a>.
     * Every version number returned will always correspond to the version after the netty rewrite, i.e.
     * it will not correspond to any minecraft versions prior to 1.7.
     * @return The protocol number the implementation is best suited for.
     */
    int getVersion();

}
