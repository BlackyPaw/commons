package com.euaconlabs.commons.core;

/**
 * This class consists of a set of constants representing the protocol version numbers
 * applying to several minecraft versions. In general a version dependent feature implementation
 * should never return protocol version numbers directly but rather one of the constants
 * declared in this class in order to prevent mischievous bugs introduced by wrong / incompatible
 * version numbers.
 *
 * @author BlackyPaw
 */
public class Protocol {

    /**
     * The version number used alongside Minecraft 1.8.
     */
    public static final int PROTOCOL_1_8 = 47;
    /**
     * The version number used from Minecraft 1.7.6 up to 1.7.10.
     */
    public static final int PROTOCOL_1_7_10 = 5;
    /**
     * The version number used from Minecraft 1.7.1 up to 1.7.5.
     */
    public static final int PROTOCOL_1_7_5 = 4;


}
