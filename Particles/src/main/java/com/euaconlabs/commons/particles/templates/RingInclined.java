package com.euaconlabs.commons.particles.templates;

import com.euaconlabs.commons.math.Quaternion;
import com.euaconlabs.commons.math.Vector3;
import com.euaconlabs.commons.particles.ParticleEffect;
import com.euaconlabs.commons.particles.ParticleEmitter;
import lombok.Getter;
import lombok.Setter;

/**
 * An implementation of the 2-sided vertically mirrored rings.
 *
 * @author BlackyPaw
 */
public class RingInclined implements ParticleEmitter {

    private static final Vector3 INCLINED_AXIS_Z = new Vector3( 1.0D, 1.0D, 0.0D );
    private static final Vector3 BASE_SEGMENT_Z = new Vector3( 0.0D, 0.0D, -1.0D );

    // ---------------- Members
    @Getter private ParticleEffect effect = null;
    @Getter private double angle = 0.0F;
    @Getter @Setter private double speed = 5.0F;
    @Getter @Setter private Vector3 location = new Vector3();

    // ---------------- Constructors
    public RingInclined( ParticleEffect effect ) {
        this.effect = effect;
    }

    // ---------------- Emitter
    public void onUpdate() {

        Quaternion q = Quaternion.angleAxis( this.angle, INCLINED_AXIS_Z );

        Vector3 p1 = q.rotate( BASE_SEGMENT_Z );
        Vector3 p2 = p1.mul( -1.0D, 1.0D, 1.0D );

        p1 = p1.add( this.location );
        p2 = p2.add( this.location );

        this.effect.broadcast( p1, 0.0F, 5 );
        this.effect.broadcast( p2, 0.0F, 5 );

        this.angle += this.speed;

    }

    public long getDelay() {
        return 0L;
    }

    public long getPeriod() {
        return 5L;
    }

}
