package com.euaconlabs.commons.particles;

import com.euaconlabs.commons.math.Vector3;

/**
 * @author BlackyPaw
 */
public interface ParticleEmitter {

    /**
     * Gets the delay of the emitter in ticks.
     * @return
     */
    long getDelay();
    /**
     * Gets the period of the emitter in ticks.
     * @return
     */
    long getPeriod();

    /**
     * Sets the location of the particle emitter.
     * @param location
     */
    void setLocation( Vector3 location );

    /**
     * Gets the location of the particle emitter.
     * @return
     */
    Vector3 getLocation();

    /**
     * Emits more particles.
     */
    void onUpdate();

}
