package com.euaconlabs.commons.particles;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.euaconlabs.commons.math.Vector3;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;

/**
 * @author BlackyPaw
 */
public enum ParticleEffect {

    // ----------------------- Enumeration Constants
    EXPLODE ( "explode" ),
    LARGE_EXPLODE ( "largeexplode" ),
    HUGE_EXPLOSION ( "hugeexplosion" ),
    FIREWORKS_SPARK ( "fireworksSpark" ),
    BUBBLE ( "bubble" ),
    SPLASH ( "splash" ),
    WAKE ( "wake" ),
    UNDERWATER ( "suspended" ),
    VOID ( "depthsuspend" ),
    CRITICAL ( "crit" ),
    CRITICAL_MAGIC ( "magicCrit" ),
    SMOKE ( "smoke" ),
    LARGE_SMOKE ( "largesmoke" ),
    SPELL ( "spell" ),
    INSTANT_SPELL ( "instantSpell" ),
    MOB_SPELL ( "mobSpell" ),
    MOB_SPELL_AMBIENT ( "mobSpellAmbient" ),
    WITCH_MAGIC ( "witchMagic" ),
    WATER_DRIP ( "dripWater" ),
    LAVE_DRIP ( "dripLava" ),
    ANGRY_VILLAGER ( "angryVillager" ),
    HAPPY_VILLAGER ( "happyVillager" ),
    TOWN_AURA ( "townaura" ),
    NOTE ( "note" ),
    PORTAL ( "portal" ),
    ENCHANTMENTTABLE ( "enchantmenttable" ),
    FLAME ( "flame" ),
    LAVA ( "lava" ),
    FOOTSTEP ( "footstep" ),
    REDDUST ( "reddust" ),
    SNOWBALL_POOF ( "snowballpoof" ),
    SLIME ( "slime" ),
    HEART ( "heart" ),
    BARRIER ( "barrier" ),
    CLOUD ( "cloud" ),
    SNOW_SHOVEL ( "snowshovel" ),
    RAIN ( "rain" ),
    TAKE ( "take" ),
    MOB_APPEARANCE ( "mobappearance" ),
    ICON_CRACK ( "iconcrack_%" ),
    BLOCK_CRACK ( "blockcrack_%_%" ),
    BLOCK_DUST ( "blockdust_%_%" );

    // ----------------------- Members
    @Getter private String name;
    @Getter @Setter private int id;
    @Getter @Setter private int data;

    // ----------------------- Constructors
    private ParticleEffect ( String name ) {
        this.name = name;
    }

    /**
     * Creates a packet resembling the particle effect.
     * @param location
     * @param speed
     * @param count
     * @return
     */
    private PacketContainer createPacket ( Location location, float speed, int count ) {
        PacketContainer handle = new PacketContainer( PacketType.Play.Server.WORLD_PARTICLES );
        handle.getModifier().writeDefaults();

        String name = this.name;
        switch ( this ) {
            case ICON_CRACK:
                name = name.replace( "%", String.valueOf( this.id ) );
                break;
            case BLOCK_CRACK:
                name = name.replace( "%", String.valueOf( this.id ) ).replace( "%", String.valueOf( this.data ) );
                break;
            case BLOCK_DUST:
                name = name.replace( "%", String.valueOf( this.id ) ).replace( "%", String.valueOf( this.data ) );
                break;
        }

        handle.getStrings().write( 0, this.name );
        handle.getFloat().write( 0, (float) location.getX() );
        handle.getFloat().write( 1, (float) location.getY() );
        handle.getFloat().write( 2, (float) location.getZ() );
        handle.getFloat().write( 3, 0.0F );
        handle.getFloat().write( 4, 0.0F );
        handle.getFloat().write( 5, 0.0F );
        handle.getFloat().write( 6, speed );
        handle.getIntegers().write( 0, count );

        return handle;
    }

    /**
     * Creates a packet resembling the particle effect.
     * @param location
     * @param speed
     * @param count
     * @return
     */
    private PacketContainer createPacket ( Vector3 location, float speed, int count ) {
        PacketContainer handle = new PacketContainer( PacketType.Play.Server.WORLD_PARTICLES );
        handle.getModifier().writeDefaults();

        String name = this.name;
        switch ( this ) {
            case ICON_CRACK:
                name = name.replace( "%", String.valueOf( this.id ) );
                break;
            case BLOCK_CRACK:
                name = name.replace( "%", String.valueOf( this.id ) ).replace( "%", String.valueOf( this.data ) );
                break;
            case BLOCK_DUST:
                name = name.replace( "%", String.valueOf( this.id ) ).replace( "%", String.valueOf( this.data ) );
                break;
        }

        handle.getStrings().write( 0, this.name );
        handle.getFloat().write( 0, (float) location.getX() );
        handle.getFloat().write( 1, (float) location.getY() );
        handle.getFloat().write( 2, (float) location.getZ() );
        handle.getFloat().write( 3, 0.0F );
        handle.getFloat().write( 4, 0.0F );
        handle.getFloat().write( 5, 0.0F );
        handle.getFloat().write( 6, speed );
        handle.getIntegers().write( 0, count );

        return handle;
    }

    /**
     * Spawns the particle effect to the given player.
     * @param player The player to spawn the particle effect to.
     * @param location The location to spawn the particles at.
     * @param speed The speed of the particles.
     * @param count The number of particles to spawn.
     */
    public void spawn ( Player player, Location location, float speed, int count ) {
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket( player, createPacket( location, speed, count ) );
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Broadcast the particle effect to all particles.
     * @param location The location to spawn the particles at.
     * @param speed The speed of the particles.
     * @param count The number of particles to spawn.
    */
    public void broadcast ( Location location, float speed, int count ) {
        ProtocolLibrary.getProtocolManager().broadcastServerPacket( createPacket( location, speed, count ) );
    }

    /**
     * Spawns the particle effect to the given player.
     * @param player The player to spawn the particle effect to.
     * @param location The location to spawn the particles at.
     * @param speed The speed of the particles.
     * @param count The number of particles to spawn.
     */
    public void spawn ( Player player, Vector3 location, float speed, int count ) {
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket( player, createPacket( location, speed, count ) );
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Broadcast the particle effect to all particles.
     * @param location The location to spawn the particles at.
     * @param speed The speed of the particles.
     * @param count The number of particles to spawn.
     */
    public void broadcast ( Vector3 location, float speed, int count ) {
        ProtocolLibrary.getProtocolManager().broadcastServerPacket( createPacket( location, speed, count ) );
    }

}
