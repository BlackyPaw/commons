package com.euaconlabs.commons.math;

import lombok.Getter;
import lombok.experimental.Accessors;

/**
 * @author BlackyPaw
 */
@Accessors( chain=true )
public class Quaternion implements Cloneable {

    // --------------------- Members
    @Getter private double s = 1.0D;
    @Getter private double x = 0.0D;
    @Getter private double y = 0.0D;
    @Getter private double z = 0.0D;

    // --------------------- Constructors
    public Quaternion() {}
    public Quaternion( double s, double x, double y, double z ) {
        this.s = s;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Quaternion( double s, Vector3 v ) {
        this.s = s;
        this.x = v.getX();
        this.y = v.getY();
        this.z = v.getZ();
    }

    // --------------------- Methods

    /**
     * Calculates the quaternion corresponding to a rotation about the given angle around the given axis.
     * @param angle The angle to rotate about in radians.
     * @param axis The axis to rotate around.
     * @return The quaternion resembling the rotation. It will be normalized.
     */
    public static Quaternion angleAxis ( double angle, Vector3 axis ) {

        double halfAngle = angle * 0.5D;
        double cos = Math.cos( halfAngle );
        double sin = Math.sin( halfAngle );

        return new Quaternion(
                cos,
                axis.getX() * sin,
                axis.getY() * sin,
                axis.getZ() * sin
        ).normalize();

    }

    /**
     * Calculates the conjugate of the quaternion.
     * @return The quaternion's conjugate.
     */
    public Quaternion conjugate() {
        return new Quaternion( this.s, -this.x, -this.y, -this.z );
    }

    /**
     * Calculates the dot product between this and the given quaternion.
     * @param q Another quaternion.
     * @return The value of the dot product of the two quaternions.
     */
    public double dot ( Quaternion q ) {
        return ( this.s * q.s + this.x * q.x + this.y * q.y + this.z * q.z );
    }

    /**
     * Calculates the angle between this and the given quaternion in radians.
     * @param q Another quaternion.
     * @return The angle between this and the given quaternion in radians.
     */
    public double angle ( Quaternion q ) {
        return Math.acos( ( dot ( q ) ) / ( magnitude() + q.magnitude() ) );
    }

    /**
     * If it is guaranteed that both quaternions are of unit length (i.e. normalized) a faster
     * method for calculating the angle between those two may be used.
     * @param q Another quaternion of unit length.
     * @return The angle between this and the given quaternion in radians.
     */
    public double angleNormalized ( Quaternion q ) {
        return Math.acos( dot ( q ) );
    }

    /**
     * Adds the given quaternion to this one and returns the resulting quaternion.
     * @param q Another quaternion.
     * @return The resulting quaternion.
     */
    public Quaternion add ( Quaternion q ) {
        return new Quaternion(
                this.s + q.s,
                this.x + q.x,
                this.y + q.y,
                this.z + q.z
        );
    }

    /**
     * Subtracts the given quaternion from this one and returns the resulting quaternion.
     * @param q Another quaternion.
     * @return The resulting quaternion.
     */
    public Quaternion sub ( Quaternion q ) {
        return new Quaternion(
                this.s - q.s,
                this.x - q.x,
                this.y - q.y,
                this.z - q.z
        );
    }

    /**
     * Multiplies this quaternion with the given quaternion.
     * @param q Another quaternion.
     * @return The result of the multiplication.
     */
    public Quaternion mul ( Quaternion q ) {
        return new Quaternion(
                ( q.s * this.s - q.x * this.x - q.y * this.y - q.z * this.z ),
                ( q.s * this.x + q.x * this.s - q.y * this.z + q.z * this.y ),
                ( q.s * this.y + q.x * this.z + q.y * this.s - q.z * this.x ),
                ( q.s * this.z - q.x * this.y + q.y * this.x + q.z * this.s )
        );
    }

    /**
     * Multiplies this quaternion with the given scalar.
     * @param s The scalar to multiply with.
     * @return The resulting quaternion.
     */
    public Quaternion mul ( double s ) {
        return new Quaternion(
                this.s * s,
                this.x * s,
                this.y * s,
                this.z * z
        );
    }

    /**
     * Calculates the inverse of this quaternion.
     * @return The inverse quaternion.
     */
    public Quaternion inverse() {
        Quaternion conjugate = conjugate();
        double magSquared = 1.0D / magnitudeSquared();

        conjugate.s *= magSquared;
        conjugate.x *= magSquared;
        conjugate.y *= magSquared;
        conjugate.z *= magSquared;

        return conjugate;
    }

    /**
     * If this quaternion is normalized a faster way may be used to calculate the quaternion's inverse.
     * @return The quaternions inverse.
     */
    public Quaternion inverseNormalized() {
        return conjugate();
    }

    /**
     * Calculates the quaternion's magnitude.
     * @return The magnitude of the quaternion.
     */
    public double magnitude () {
        return Math.sqrt( this.s * this.s + this.x * this.x + this.y * this.y + this.z * this.z );
    }

    /**
     * Calculates the quaernion's squared magnitude which is less expensive than the actual magnitude.
     * @return The squared magnitude of the quaternion.
     */
    public double magnitudeSquared () {
        return ( this.s * this.s + this.x * this.x + this.y * this.y + this.z * this.z );
    }

    /**
     * Rotates the given vector using this quaternion.
     * @param v The vector to rotate.
     * @return The rotated vector.
     */
    public Vector3 rotate ( Vector3 v ) {
        Quaternion p = new Quaternion( 0.0D, v.getX(), v.getY(), v.getZ() );
        Quaternion result = mul( p ).mul( inverse() );
        return new Vector3( result.x, result.y, result.z);
    }

    /**
     * Linearly interpolates between this and the given quaternion. Note that for interpolating quaternions
     * they must be normalized.
     * @param t The percentage to interpolate ( 0.0 - 1.0 ).
     * @param q The quaternion to interpolate with.
     * @return The interpolated quaternion. It will be normalized.
     */
    public Quaternion lerp( double t, Quaternion q ) {
        if ( t < 0.0D || t > 1.0D )
            throw new AssertionError( "Parameter t for linear interpolation is out of range: 0.0 - 1.0!" );

        return mul( ( 1.0D - t ) ).add( q.mul( t ) ).normalize();
    }

    /**
     * Spherically interpolates between this and the given quaternion. Note that for interpolating quaternions
     * they must be normalized.
     *
     * The slerp function will generate smoother results than its lerp counterpart but is a lot more expensive.
     * Therefore it is not supposed to be called repeatedly.
     * @param t The percentage to interpolate ( 0.0 - 1.0 ).
     * @param q The quaternion to interpolate with.
     * @return The interpolated quaternion. It will be normalized.
     */
    public Quaternion slerp ( double t, Quaternion q ) {
        if ( t < 0.0D || t > 1.0D )
            throw new AssertionError( "Parameter t for linear interpolation is out of range: 0.0 - 1.0!" );

        double dot = this.dot( q );
        double sin = Math.sqrt( 1.0D - dot * dot );

        return mul( ( sin * ( 1.0D - t ) / sin ) ).add( q.mul( ( sin * t / sin ) ) ).normalize();
    }

    /**
     * If the quaternion is normalized to unit length a faster method for rotating a vector using
     * it may be used.
     * @param v The vector to rotate.
     * @return The rotated vector.
     */
    public Vector3 rotateNormalized ( Vector3 v ) {
        Vector3 p = new Vector3( this.x, this.y, this.z );
        Vector3 t = p.cross( v ).mul( 2.0D );
        return v.clone().add( t.mul( this.s ) ).add( p.cross( t ) );
    }

    /**
     * Returns the quaternion resembling this one but being normalized to unit length.
     * @return The normalized quaternion.
     */
    public Quaternion normalize () {
        double mag = 1.0D / magnitude();
        return new Quaternion(
                this.s * mag,
                this.x * mag,
                this.y * mag,
                this.z * mag
        );
    }

    /**
     * Tests whether or not two quaternions are equal to each other.
     * @param o The object to compare against.
     * @return True if the two quaternions are equal; False otherwise.
     */
    @Override public boolean equals ( Object o ) {
        if ( this == o )
            return true;

        if ( ! ( o instanceof Quaternion ) )
            return false;

        Quaternion q = (Quaternion) o;
        return ( Double.compare( this.s, q.s ) == 0 && Double.compare( this.x, q.x ) == 0 &&
                 Double.compare( this.y, q.y ) == 0 && Double.compare( this.z, q.z ) == 0 );
    }

    /**
     * Calculates a hash code of the quaternion.
     * @return The calculated hash code.
     */
    @Override public int hashCode() {
        int result = 17;

        long ls = Double.doubleToLongBits( this.s );
        long lx = Double.doubleToLongBits( this.x );
        long ly = Double.doubleToLongBits( this.y );
        long lz = Double.doubleToLongBits( this.z );

        result = 31 * result + ( (int) ( ls ^ ( ls >> 32 ) ) );
        result = 31 * result + ( (int) ( lx ^ ( lx >> 32 ) ) );
        result = 31 * result + ( (int) ( ly ^ ( ly >> 32 ) ) );
        result = 31 * result + ( (int) ( lz ^ ( lz >> 32 ) ) );

        return result;
    }

    /**
     * Clones this quaternion.
     * @return The cloned quaternion.
     */
    @Override public Quaternion clone() {
        try {
            Quaternion q = (Quaternion) super.clone();
            q.s = this.s;
            q.x = this.x;
            q.y = this.y;
            q.z = this.z;
            return q;
        } catch ( CloneNotSupportedException e ) {
            throw new AssertionError( ); // Cannot happen.
        }
    }

    /**
     * Converts the quaternion into its string representation which corresponds to: s,x,y,z
     * @return The string representation of the quaternion.
     */
    @Override public String toString() {
        return String.format( "%f,%f,%f,%f", this.s, this.x, this.y, this.z );
    }

}
