package com.euaconlabs.commons.math;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author BlackyPaw
 */
@Accessors( chain=true )
public class Vector3 implements Cloneable {

    @Getter @Setter private double x = 0.0F;
    @Getter @Setter private double y = 0.0F;
    @Getter @Setter private double z = 0.0F;

    // ----------------------- Constructors
    public Vector3() {}
    public Vector3( int x, int y, int z ) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Vector3( float x, float y, float z ) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Vector3( double x, double y, double z ) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Vector3( org.bukkit.util.Vector v ) {
        this.x = v.getX();
        this.y = v.getY();
        this.z = v.getZ();
    }

    // ----------------------- Accessors
    public Vector3 setX ( int x ) {
        this.x = x;
        return this;
    }

    public Vector3 setX ( float x ) {
        this.x = x;
        return this;
    }

    public Vector3 setY ( int y ) {
        this.y = y;
        return this;
    }

    public Vector3 setY ( float y ) {
        this.y = y;
        return this;
    }

    public Vector3 setZ ( int z ) {
        this.z = z;
        return this;
    }

    public Vector3 setZ ( float z ) {
        this.z = z;
        return this;
    }

    // ----------------------- Methods
    public static Vector3 valueOf ( org.bukkit.util.Vector v ) {
        return new Vector3 ( v.getX(), v.getY(), v.getZ() );
    }

    public Vector3 add( int s ) {
        return new Vector3( this.x + s, this.y + s, this.z + s );
    }

    public Vector3 add( float s ) {
        return new Vector3( this.x + s, this.y + s, this.z + s );
    }

    public Vector3 add( double s ) {
        return new Vector3( this.x + s, this.y + s, this.z + s );
    }

    public Vector3 add ( int x, int y, int z ) { return new Vector3( this.x + x, this.y + y, this.z + z ); }

    public Vector3 add ( float x, float y, float z ) { return new Vector3( this.x + x, this.y + y, this.z + z ); }

    public Vector3 add ( double x, double y, double z ) { return new Vector3( this.x + x, this.y + y, this.z + z ); }

    public Vector3 add( Vector3 v ) {
        return new Vector3( this.x + v.x, this.y + v.y, this.z + v.z);
    }

    public Vector3 sub( int s ) {
        return new Vector3( this.x - s, this.y - s, this.z - s );
    }

    public Vector3 sub( float s ) {
        return new Vector3( this.x - s, this.y - s, this.z - s );
    }

    public Vector3 sub( double s ) {
        return new Vector3( this.x - s, this.y - s, this.z - s );
    }

    public Vector3 sub ( int x, int y, int z ) { return new Vector3( this.x - x, this.y - y, this.z - z ); }

    public Vector3 sub ( float x, float y, float z ) { return new Vector3( this.x - x, this.y - y, this.z - z ); }

    public Vector3 sub ( double x, double y, double z ) { return new Vector3( this.x - x, this.y - y, this.z - z ); }

    public Vector3 sub( Vector3 v ) {
        return new Vector3( this.x - v.x, this.y - v.y, this.z - v.z);
    }

    public Vector3 mul( int s ) {
        return new Vector3( this.x * s, this.y * s, this.z * s );
    }

    public Vector3 mul( float s ) {
        return new Vector3( this.x * s, this.y * s, this.z * s );
    }

    public Vector3 mul( double s ) {
        return new Vector3( this.x * s, this.y * s, this.z * s );
    }

    public Vector3 mul ( int x, int y, int z ) { return new Vector3( this.x * x, this.y * y, this.z * z ); }

    public Vector3 mul ( float x, float y, float z ) { return new Vector3( this.x * x, this.y * y, this.z * z ); }

    public Vector3 mul ( double x, double y, double z ) { return new Vector3( this.x * x, this.y * y, this.z * z ); }

    public Vector3 mul( Vector3 v ) {
        return new Vector3( this.x * v.x, this.y * v.y, this.z * v.z);
    }

    public Vector3 div( int s ) {
        return new Vector3( this.x / s, this.y / s, this.z / s );
    }

    public Vector3 div( float s ) {
        return new Vector3( this.x / s, this.y / s, this.z / s );
    }

    public Vector3 div( double s ) {
        return new Vector3( this.x / s, this.y / s, this.z / s );
    }

    public Vector3 div ( int x, int y, int z ) { return new Vector3( this.x / x, this.y / y, this.z / z ); }

    public Vector3 div ( float x, float y, float z ) { return new Vector3( this.x / x, this.y / y, this.z / z ); }

    public Vector3 div ( double x, double y, double z ) { return new Vector3( this.x / x, this.y / y, this.z / z ); }

    public Vector3 div( Vector3 v ) {
        return new Vector3( this.x / v.x, this.y / v.y, this.z / v.z);
    }

    /**
     * Calculates the dot product between this vector and the given one.
     * @param v Another vector.
     * @return The dot product between the two vectors.
     */
    public double dot ( Vector3 v ) {
        return ( this.x * v.x + this.y * v.y + this.z * v.z );
    }

    /**
     * Calculates the cross product between this vector and the given one.
     * @param v Another vector.
     * @return The cross product of the two vectors.
     */
    public Vector3 cross ( Vector3 v ) {
        return new Vector3( this.y * v.z - this.z * v.y,
                            this.z * v.x - this.x * v.z,
                            this.x * v.y - this.y * v.x );
    }

    /**
     * Calculates the magnitude of the vector.
     * @return
     */
    public double magnitude () {
        return Math.sqrt( this.x * this.x + this.y * this.y + this.z * this.z );
    }

    /**
     * Calculates the squared magnitude of the vector which is less expensive than
     * the actual magnitude.
     * @return
     */
    public double magnitudeSquared () {
        return ( this.x * this.x + this.y * this.y + this.z * this.z );
    }

    /**
     * Calculates the distance between this and the given vector.
     * @param v Another vector.
     * @return
     */
    public double distance( Vector3 v ) {
        return Math.sqrt( ( this.x - v.x ) * ( this.x - v.x) + ( this.y - v.y ) * ( this.y - v.y ) + ( this.z - v.z ) * ( this.z - v.z ) );
    }

    /**
     * Calculates the squared distance between this and the given vector.
     * @param v Another vector.
     * @return
     */
    public double distanceSquared( Vector3 v ) {
        return ( ( this.x - v.x ) * ( this.x - v.x) + ( this.y - v.y ) * ( this.y - v.y ) + ( this.z - v.z ) * ( this.z - v.z ) );
    }

    /**
     * Calculates the midpoint between this and the given vector.
     * @param v Another vector.
     * @return The vector representing the midpoint between the two vectors.
     */
    public Vector3 midpoint ( Vector3 v ) {
        return this.add( v ).mul( 0.5D );
    }

    /**
     * Calculates the angle between this and the given vector.
     * @param v Another vector.
     * @return The angle between the two vectors in radians.
     */
    public double angle( Vector3 v ) {
        return Math.acos( ( dot( v ) ) / ( magnitude() + v.magnitude() ) );
    }

    /**
     * Returns an unit vector having a magnitude of 1.
     * @return
     */
    public Vector3 normalize() {
        double mag = 1.0D / magnitude();
        return new Vector3( this.x * mag, this.y * mag, this.z * mag );
    }

    /**
     * Converts the vector into a bukkit vector.
     * @return
     */
    public org.bukkit.util.Vector toBukkit() {
        return new org.bukkit.util.Vector( this.x, this.y, this.z );
    }

    /**
     * Initializes the vector given a bukkit vector.
     * @param v The bukkit vector.
     * @return
     */
    public Vector3 fromBukkit ( org.bukkit.util.Vector v ) {
        this.x = v.getX();
        this.y = v.getY();
        this.z = v.getZ();
        return this;
    }

    /**
     * Tests whether or not the given object is equal to this vector.
     * @param o
     * @return
     */
    @Override public boolean equals( Object o ) {
        if ( this == o )
            return true;

        if ( ! ( o instanceof Vector3 ) )
            return false;

        Vector3 v = (Vector3) o;
        return ( Double.compare( this.x, v.x ) == 0 && Double.compare( this.y, v.y ) == 0 && Double.compare( this.z, v.z ) == 0 );
    }

    /**
     * Calculates a hash code of the vector.
     * @return The vector's hash code.
     */
    @Override public int hashCode() {
        int result = 17;

        long lx = Double.doubleToLongBits( this.x );
        long ly = Double.doubleToLongBits( this.y );
        long lz = Double.doubleToLongBits( this.z );

        result = 31 * result + ( (int) ( lx ^ ( lx >> 32 ) ) );
        result = 31 * result + ( (int) ( ly ^ ( ly >> 32 ) ) );
        result = 31 * result + ( (int) ( lz ^ ( lz >> 32 ) ) );

        return result;
    }

    /**
     * Clones this vector.
     * @return
     */
    @Override public Vector3 clone() {
        try {
            Vector3 v = (Vector3) super.clone();
            v.x = this.x;
            v.y = this.y;
            v.z = this.z;
            return v;
        } catch( CloneNotSupportedException e ) {
            throw new AssertionError( ); // Cannot happen.
        }
    }

    /**
     * Converts the vector into its string representation: x,y,z
     * @return
     */
    @Override public String toString() {
        return String.format( "%f,%f,%f", this.x, this.y, this.z );
    }

}
